import requests
import logging
from bs4 import BeautifulSoup
from Core.Logger import log

requestHeader = {"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                 "Accept-Encoding": "gzip, deflate, sdch, br",
                 "Accept-Language": "zh-CN,zh;q=0.8",
                 "Cache-Control": "max-age=0",
                 "Host": "www.zhihu.com",
                 "Upgrade-Insecure-Requests": "1",
                 "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko)"
                               " Chrome/56.0.2924.87 Safari/537.36"}

loginURL = 'https://www.zhihu.com/login/email'

mainPageURL = 'https://www.zhihu.com'


# 用户账号登陆模块
class AccountManager:
    __slots__ = ('login_token', 'password', 'auth_token')

    def __init__(self, login_token, password):
        self.login_token = login_token
        self.password = password

        # 登陆凭证（Cookies）
        self.auth_token = None

    # 返回登陆凭证
    def get_auth_token(self):
        return self.auth_token

    # 登陆
    def login(self):
        # 创建会话
        session = requests.session()
        session.headers = requestHeader

        # 获取 _xsrf
        try:
            response = session.get(mainPageURL)
            input_tag = BeautifulSoup(response.text, 'html.parser').find('input', attrs={'name': '_xsrf'})
            if input_tag is None:
                return False
            _xsrf = input_tag['value']

            # login
            form_data = {'_xsrf': _xsrf,
                         'email': self.login_token,
                         'password': self.password}
            requestHeader.update({'X-Requested-With': 'XMLHttpRequest', 'X-Xsrftoken': _xsrf})
            session.headers = requestHeader
            response = session.post(url=loginURL, data=form_data)
            if response.status_code == 200:
                # 保存登陆认证cookie
                self.auth_token = {'_zap': '8d50e297-530d-4da6-915c-3968d036f797',
                                   'd_c0': 'ABCChHaChwuPTgvqXeJi-fpE0nN4LSUIxJk=|1490852457',
                                   'q_c1': 'b8c54864acac442ab5cfe2d91f8c8714|1490852450000|1490852450000',
                                   '_xsrf': 'e7df58ab3b105004ac5a8b218e38e4fe',
                                   'capsion_ticket': '2|1:0|10:1490791617|14:capsion_ticket|44:OTg4MTI3OTQyNDVkNDRiOThmMzNhYmI2ODczYmU3YWE=|c4c25adac6cf11fd8990db35387a20938549741a216a7ec46fc606a79e2f7e3e',
                                   'z_c0': 'Mi4wQUdDQ0JnOVJod3NBRUlLRWRvS0hDeGNBQUFCaEFsVk5lU2NFV1FCN1JDdTV2QzUzeHdaR05rOGFBSjdfTFUxeU9n|1490852473|0c0f8593ffbde99b7ae8552d2b5479374734d3fd',
                                   '__utma': '51854390.1886164006.1490852458.1490852458.1490852458.1',
                                   '__utmb': '51854390.0.10.1490852458',
                                   '__utmc': '51854390',
                                   '__utmv': '51854390.000--|3=entry_date=20170330=1',
                                   '__utmz': '51854390.1490852458.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)',
                                   'aliyungf_tc': 'AQAAAKktVVNyQgwAlo10ce8Uwm29d3fm',
                                   'cap_id': 'MDI0N2Y0ODQyMzNkNGE0MWEwNzk4YThmNzAwNGFmNDg=|1490852450|1cf21804558a3f3aeff36508e00f9c1fc82bbc86',
                                   'l_n_c': '1',
                                   'r_cap_id': 'NWYyMTNiYTI2NGY2NDE0OWIyMWJiYTdlMGJjNDRkNmQ=|1490852450|291a1843d8b3bb58bf53e1fc16c7d5a7f22e2107'
                                   }


                #self.auth_token = session.cookies.get_dict()
                if log.isEnabledFor(logging.INFO):
                    log.info('知乎账户登陆成功')
                return True
            else:
                if log.isEnabledFor(logging.INFO):
                    log.info('知乎账户登陆失败')
                return False
        except Exception as e:
            if log.isEnabledFor(logging.ERROR):
                log.error(e)
        finally:
            session.close()

# if __name__ == '__main__':
#     account_manager = AccountManager('xxx', 'xxx')
#     account_manager.login()
#     print(account_manager.get_auth_token())
